#pragma checksum "C:\Users\nourd\source\repos\Imdb_App\Imdb_App\Pages\Shared\_Footer.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d6de17b424efdc9415617547f9394ec0721f0fbd"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(Imdb_App.Pages.Shared.Pages_Shared__Footer), @"mvc.1.0.view", @"/Pages/Shared/_Footer.cshtml")]
namespace Imdb_App.Pages.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\nourd\source\repos\Imdb_App\Imdb_App\Pages\_ViewImports.cshtml"
using Imdb_App;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d6de17b424efdc9415617547f9394ec0721f0fbd", @"/Pages/Shared/_Footer.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4c1a93ef32b798c5941ccc2c1b8a3691245f118d", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Shared__Footer : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<footer");
            BeginWriteAttribute("class", " class=\"", 7, "\"", 15, 0);
            EndWriteAttribute();
            WriteLiteral(@">
    <div class=""bg-gray-4000"">
        <div class=""container px-md-6 px-xl-7 px-wd-6"">
            <div class=""d-flex flex-wrap align-items-center pt-6 pb-3d border-bottom mb-7 border-gray-4100"">
                <a href=""#"" class=""mb-4 mb-md-0 mr-auto"">
                    <svg version=""1.1"" width=""92"" height=""40px"">
                        <g fill=""#fff"">
                            <path class=""vodi-svg0"" d=""M72.8,12.7c0-2.7,0-1.8,0-4.4c0-0.9,0-1.8,0-2.8C73,3,74.7,1.4,77,1.4c2.3,0,4.1,1.8,4.2,4.2c0,1,0,2.1,0,3.1
                                c0,6.5,0,9.4,0,15.9c0,4.7-1.7,8.8-5.6,11.5c-4.5,3.1-9.3,3.5-14.1,0.9c-4.7-2.5-7.1-6.7-7-12.1c0.1-7.8,6.3-13.6,14.1-13.2
                                c0.7,0,1.4,0.2,2.1,0.3C71.3,12.2,72,12.4,72.8,12.7z M67.8,19.8c-2.9,0-5.2,2.2-5.2,5c0,2.9,2.3,5.3,5.2,5.3
                                c2.8,0,5.2-2.4,5.2-5.2C73,22.2,70.6,19.8,67.8,19.8z
                                M39.9,38.6c-7.3,0-13.3-6.1-13.3-13.5c0-7.5,5.9-13.4,13.4-13.4c7.5,0,13.4,6,13.4,13.5");
            WriteLiteral(@"
                                C53.4,32.6,47.4,38.6,39.9,38.6z M39.9,30.6c3.2,0,5.6-2.3,5.6-5.6c0-3.2-2.3-5.5-5.5-5.5c-3.2,0-5.6,2.2-5.6,5.4
                                C34.4,28.2,36.7,30.6,39.9,30.6z
                                M14.6,27c0.6-1.4,1.1-2.6,1.6-3.8c1.2-2.9,2.5-5.8,3.7-8.8c0.7-1.7,2-2.8,4-2.7c3,0,4.9,2.6,3.8,5.4
                                c-0.5,1.3-1.2,2.6-1.8,3.9c-2.4,5-4.9,10-7.3,15c-0.8,1.6-2,2.6-3.9,2.6c-2,0-3.3-0.8-4.2-2.6c-2.7-5.6-5.3-11.1-8-16.7
                                c-0.3-0.7-0.6-1.3-0.9-2c-0.8-1.8-0.3-3.7,1.1-4.8c1.5-1.2,4-1.3,5.3,0c0.7,0.6,1.2,1.5,1.6,2.3C11.3,18.8,12.9,22.7,14.6,27z
                                M90.9,25.1c0,3.1,0,6.2,0,9.4c0,1.9-1.2,3.4-2.9,4c-1.7,0.5-3.5,0-4.5-1.6c-0.5-0.8-0.8-1.8-0.8-2.6
                                c-0.1-6.1-0.1-11.3,0-17.5c0-2.2,1.5-3.9,3.5-4.2c2.1-0.3,4.1,0.9,4.7,2.9c0.1,0.5,0.2,1.1,0.2,1.6C90.9,20,90.9,22.1,90.9,25.1
                                C90.9,25.1,90.9,25.1,90.9,25.1z
                             ");
            WriteLiteral(@"   M90.2,4.7L86,2.3c-1.3-0.8-3,0.2-3,1.7v4.8c0,1.5,1.7,2.5,3,1.7l4.2-2.4C91.5,7.4,91.5,5.5,90.2,4.7z""></path>
                        </g>
                    </svg>
                </a>
                <ul class=""list-unstyled mx-n3 mb-0 d-flex flex-wrap align-items-center"">
                    <li class=""px-3"">
                        <a href=""#"" class=""text-gray-1300 d-flex flex-wrap align-items-center""><i class=""fa fa-facebook-f fa-inverse""></i> <span class=""ml-2"">Facebook</span></a>
                    </li>
                    <li class=""px-3"">
                        <a href=""#"" class=""text-gray-1300 d-flex flex-wrap align-items-center""><i class=""fa fa-twitter fa-inverse""></i> <span class=""ml-2"">Twitter</span></a>
                    </li>
                    <li class=""px-3"">
                        <a href=""#"" class=""text-gray-1300 d-flex flex-wrap align-items-center""><i class=""fa fa-google-plus-g fa-inverse""></i> <span class=""ml-2"">Google+</span></a>
                    </li>
         ");
            WriteLiteral(@"           <li class=""px-3"">
                        <a href=""#"" class=""text-gray-1300 d-flex flex-wrap align-items-center""><i class=""fa fa-vimeo-v fa-inverse""></i> <span class=""ml-2"">Vimeo</span></a>
                    </li>
                    <li class=""px-3"">
                        <a href=""#"" class=""text-gray-1300 d-flex flex-wrap align-items-center""><i class=""fa fa-rss fa-inverse""></i> <span class=""ml-2"">RSS</span></a>
                    </li>
                </ul>
            </div>
            <div class=""row pb-5"">
                <div class=""col-md mb-5 mb-md-0"">
                    <h4 class=""font-size-18 font-weight-medium mb-4 text-gray-4200"">Movie Categories</h4>
                    <ul class=""column-count-2 v2 list-unstyled mb-0"">
                        <li class=""py-1d"">
                            <a class=""h-g-white"" href=""https://demo2.madrasthemes.com/vodi-html/html-demo/archive/movies.html"">Action</a>
                        </li>
                        <li class=""py-1");
            WriteLiteral(@"d"">
                            <a class=""h-g-white"" href=""https://demo2.madrasthemes.com/vodi-html/html-demo/archive/movies.html"">Adventure</a>
                        </li>
                        <li class=""py-1d"">
                            <a class=""h-g-white"" href=""https://demo2.madrasthemes.com/vodi-html/html-demo/archive/movies.html"">Animation</a>
                        </li>

                    </ul>
                </div>
                <div class=""col-md mb-5 mb-md-0"">
                    <h4 class=""font-size-18 font-weight-medium mb-4 text-gray-4200"">TV Series</h4>
                    <ul class=""column-count-2 v2 list-unstyled mb-0"">
                        <li class=""py-1d"">
                            <a class=""h-g-white"" href=""https://demo2.madrasthemes.com/vodi-html/html-demo/archive/tv-shows.html"">Valentine Day</a>
                        </li>
                        <li class=""py-1d"">
                            <a class=""h-g-white"" href=""https://demo2.madrasthemes.com/v");
            WriteLiteral(@"odi-html/html-demo/archive/tv-shows.html"">Underrated Comedies</a>
                        </li>
                        <li class=""py-1d"">
                            <a class=""h-g-white"" href=""https://demo2.madrasthemes.com/vodi-html/html-demo/archive/tv-shows.html"">Scary TV Series</a>
                        </li>

                    </ul>
                </div>
                <div class=""col-md-2 mb-5 mb-md-0 border-left border-gray-4100"">
                    <div class=""ml-1"">
                        <h4 class=""font-size-18 font-weight-medium mb-4 text-gray-4200"">Support</h4>
                        <ul class=""list-unstyled mb-0"">
                            <li class=""py-1d"">
                                <a class=""h-g-white"" href=""https://demo2.madrasthemes.com/vodi-html/html-demo/static/contact.html"">My Account</a>
                            </li>
                            <li class=""py-1d"">
                                <a class=""h-g-white"" href=""https://demo2.madrasthemes.com");
            WriteLiteral(@"/vodi-html/html-demo/static/contact.html"">FAQ</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=""bg-gray-4300"">
        <div class=""container px-md-6 px-xl-7 px-wd-6s"">
            <div class=""text-center d-md-flex flex-wrap align-items-center py-3"">
                <div class=""font-size-13 text-gray-1300 mb-2 mb-md-0"">Copyright © 2020, Vodi. All Rights Reserved</div>
                <a href=""https://demo2.madrasthemes.com/vodi-html/html-demo/static/contact.html"" class=""font-size-13 h-g-white ml-md-auto"">Privacy Policy</a>
            </div>
        </div>
    </div>
</footer>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
